<?php namespace api\MailService;

// $incHddr=getenv("incHddr");
// $fl="$incHddr/include/mail/PHPMailer/src/PHPMailer.php";
// include_once($fl);

use PHPMailer\PHPMailer\PHPMailer;

class MailService 
{
    // private $returns="";
    // private $postsArray=[];
    // private $myName="MailService";
    // private $validCallersArray=['AppControlBL','QuizLogic'];
    // private $logFileLocation="/home/skunksn/logs/mail/";
    // private $deBug=false;
    // public function __construct()
    // {
    //     $this->deBug=getenv('debug');
    //     if (!isset($_POST['mail']['caller'])) {
    //         $this->returns="err|noKey";
    //     }
    //     if ($this->returns=="") {
    //         $this->postsArray=$_POST['mail'];
    //         unset($_POST['mail']);
    //         $valid=$this->validateCaller();
    //         if(!$valid){
    //             if($this->deBug)
    //             {
    //                 echo("<br>Failure here::".__LINE__."::deBug::".$this->deBug);
    //                 exit();
    //             }
    //             echo("<br>Failure on Security");
    //             header("Location:index.php");
    //         }
    //         $this->sendIt();
    //         if (!$this->postsArray['SendStatus']) {
    //             $this->returns="err|noSend";
    //         }
    //     }
    //     //$this->writeLog();
    // }
    // public function getMailResult() : string
    // {
    //     $_SESSION['codeTrace'][]=$this->myName.'.'.__FUNCTION__."()::public";
    //     return $this->returns;
    // }
    public static function sendMailSimple($sendArray)
    {
        /*
            ['nameFrom']
            ['to'][][]
            ['subject']
            ['body']

        */
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->Host = getenv('smtpServer');
        $mail->Port = getenv('smptpPort');
        $mail->SMTPSecure = getenv('smtpSecure');
        $mail->SMTPAuth = getenv('smtpAuth');
        $mail->Username = getenv('smtpUser');
        $mail->Password = getenv('smtpPassword');
        $mail->setFrom(getenv('smtpUser'), $sendArray['nameFrom']);
        $mail->addReplyTo(getenv('smtpUser'), $sendArray['nameFrom']);        
        $mail->addAddress($sendArray['to'][0], $sendArray['nameFrom']);
        foreach ($sendArray['to'] as $key => $value) {
            $mail->addAddress($value, "");
        }
        $renderAt=date("Y-m-d H:i:s");
        $body=$sendArray['body']."<br><br><br>Sent at:<b> $renderAt </b><br>";
        $mail->msgHTML($body);
        $mail->Subject=$sendArray['subject'];
        //$mail->msgHTML(urldecode($this->postsArray['body']));
        if (!$mail->send()) {
            $retArray['err'][]="Sending Error {$mail->ErrorInfo}";
            $retArray['err'][]=$mail->ErrorInfo;
            return $retArray;
        }
        return true;
    }
}
