<?php

return [

   'default' => 'accounts',

   'connections' => [
        'mysql' => [
            'driver'    => 'mysql',
            'host'      => env('DB_HOST'),
            'port'      => env('DB_PORT'),
            'database'  => env('DB_DATABASE'),
            'username'  => env('DB_USERNAME'),
            'password'  => env('DB_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
         ],
         // This is unset for deactivating AppControl
        // 'mysql2' => [
        //     'driver'    => 'mysql',
        //     'host'      => env('DB_HOST_APPCONTROL'),
        //     'port'      => env('DB_PORT_APPCONTROL'),
        //     'database'  => env('DB_DATABASE_APPCONTROL'),
        //     'username'  => env('DB_USERNAME_APPCONTROL'),
        //     'password'  => env('DB_PASSWORD_APPCONTROL'),
        //     'charset'   => 'utf8',
        //     'collation' => 'utf8_unicode_ci',
        //     'prefix'    => '',
        //     'strict'    => false,
        // ],
    ],
];