<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;
use App\Http\Controllers\AppControl;
use App\Http\Controllers\ControllerHelper as Helper;
use PhpParser\JsonDecoder;

use function PHPUnit\Framework\isNull; //probably not used here 

class ProductController extends Controller
{
    public function listProducts(Request $request) 
    {
        $productGetArray=ControllerHelper::validateProductGet($request);
        return response()->json($productGetArray['data'],(int)$productGetArray['status'])
        ->header('Content-Type', 'json')
        ->header('status', $productGetArray['header']['status'])
        ->header('notice', $productGetArray['header']['notice'])
        ->header('Doc', "UseOfShortTermToken"); 
    }
    public function createProduct(Request $request){
        $productPostArray=ControllerHelper::validateProductPost($request);
        return response()->json($productPostArray['data'],(int)$productPostArray['status'])
        ->header('Content-Type', 'json')
        ->header('status', $productPostArray['header']['status'])
        ->header('notice', $productPostArray['header']['notice'])
        ->header('Doc', "UseOfShortTermToken"); 
        
    }
}