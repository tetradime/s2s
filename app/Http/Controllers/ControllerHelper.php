<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use JsonIncrementalParser;

use function Deployer\Support\array_flatten;
use function PHPUnit\Framework\isNull;

/* USAGE:: assists with all controllers - generic validation & dbAccess for general calls*/
class ControllerHelper 
{
    static $myName="ControllerHelper";
    static $limit=10;  //default records per page
    static $offset=1;  //default page
    static $docExplained="0->not mandated;1->mandated | type | maximum lenght";
    static $productDisplayArray=array('sku','attributes');
    public static function validateProductPost(object $request)                        //02     01
    {
        $productValidCaptures=self::getProductPosts();
        $evalJson=false;
        if ($request->isJson()) {
            $evalJson=true;
        }
        if($evalJson){
            $inputs = $request->json()->all();
            if(count($inputs)==0){
                $response['status']=400;
                $response['data']['msg']="({$response['status']})Header(application/json) set but no valid json POST";
                $response['header']['status']=$response['status'];
                $response['header']['notice']=$response['data']['msg'];
                return $response;
            }
            $inputKeys=array_keys($inputs);
            foreach ($productValidCaptures as $key => $value) {
                $itemArray=explode("|",$value);
                if($itemArray[0]==1){ //mandated
                    if(!isset($inputs[$key])){
                        $response['status']=412;
                        $response['data']['msg']="({$response['status']}) $key was not specified";
                        $response['header']['status']=$response['status'];
                        $response['header']['notice']=$response['data']['msg'];
                        return $response;
                    }
                }               
                if(isset($inputs[$key])){       //if it is set -> chekc it
                    if($itemArray[1]=='str'){ //check length
                        $maxLen=(int)$itemArray[2];
                        if(strlen($inputs[$key])>$maxLen){
                            $response['status']=412;
                            $response['data']['msg']="({$response['status']}) $key value exceeded max lenght of ($itemArray[2])";
                            $response['header']['status']=$response['status'];
                            $response['header']['notice']=$response['data']['msg'];
                            return $response;
                        }
                    }
                    // same checks could be done for date,float and int type fields
                    // not in this spec
                }                
                $validatedInputs[$key]=$inputs[$key];
            }
            //check if record exists by by sku
            //create the record
            $exists=self::getProducts(["sku"=>$validatedInputs['sku']]);
            //dd($exists);
            if(isset($exists[0])){
                $response['status']=409;
                $response['data']['msg']="({$response['status']}) sku={$validatedInputs['sku']} exists already";
                $response['header']['status']=$response['status'];
                $response['header']['notice']=$response['data']['msg'];
                return $response;                
            }
            if(is_array($validatedInputs['attributes'])){
                $attr="{";
                foreach ($validatedInputs['attributes'] as $key => $value) {
                    $attr.="\"$key\" : \"$value\",";
                }
                $attr=substr($attr,0,strlen($attr)-1);
                $attr.="}";
                $validatedInputs['attributes']=$attr;
            }
            $newRecordId=self::createProduct($validatedInputs);
            if($newRecordId>0){
                $response['status']=201;
                $response['data']['msg']="({$response['status']}) sku={$validatedInputs['sku']} created";
                $response['header']['status']=$response['status'];
                $response['header']['notice']="Call get /productes/id/$newRecordId to validate";
                return $response;    
            }
            //FUBAR - not sure what would create this error
            $response['status']=503;
            $response['data']="";
            $response['header']['status']=$response['status'];
            $response['header']['notice']="Indeterminate Error";
        }
        $product=self::getProducts();
        //$fullProductSet=self::setAttributesOffProductArrray($product);
        $response['status']=200;
        $cnt=count($product);
        $response['data']=$product;
        $response['header']['status']=$response['status'];
        $response['header']['notice']="(RecordCount=$cnt)";
        return $response;   
    }    
    public static function validateProductGet(object $request)                        //02     01
    {
        $productValidFilters=self::getProductGets();
        $evalJson=false;
        if ($request->isJson()) {
            $evalJson=true;
        }
        if($evalJson){
            $inputs = $request->json()->all();
            if(count($inputs)==0){      // no filter json set but header informs that
                $response['status']=400;
                $response['data']['msg']="({$response['status']})application/json set but no valid json present";
                $response['header']['status']=$response['status'];
                $response['header']['notice']=$response['data']['msg'];
                return $response;
            }
            if(count($inputs)>1){       //multiple fiters set
                $response['status']=400;
                $response['data']['msg']="({$response['status']})to many filters set in Json => one of id,sku)";
                $response['header']['status']=$response['status'];
                $response['header']['notice']=$response['data']['msg'];
                return $response;
            }
            $key=(string)array_keys($inputs)[0];
            if(isset($productValidFilters[$key])){
                $product=self::getProducts($inputs);
                if(count($product)==0){    //no recs found
                    $response['status']=404;
                    $response['data']['0']="";
                    $response['header']['status']=$response['status'];
                    $response['header']['notice']='No Records Found';
                    return $response;                     
                }
                $response['status']=200;
                $response['data']=$product;
                $cnt=count($product);
                $response['header']['status']=$response['status'];
                $response['header']['notice']="(RecordCount=$cnt)";
                return $response;               
            }
        }
        $product=self::getProducts();
        //$fullProductSet=self::setAttributesOffProductArrray($product);
        $response['status']=200;
        $cnt=count($product);
        $response['data']=$product;
        $response['header']['status']=$response['status'];
        $response['header']['notice']="(RecordCount=$cnt)";
        return $response;   
    }
    private static function createProduct(array $validatedInputs)
    {
        $id = DB::connection('mysql')->table('products')->insertGetId(
            $validatedInputs
        );
        return $id;
    }
    private static function getProducts(array $inputs=null)
    {
        $data=DB::connection('mysql')->table('products')
            ->select('id','sku','attributes','status','updated','created')
            ->where($inputs)
            ->get();
        return $data;
    }
    // these two next fratures are usually stored in the database 
    private static function getProductPosts()                        //02     01
    {      /* => nonMandated(0)|Data type|Maxsize */
        $product['sku']         ='1|str|16';
        $product['attributes']  ='1|json|100000';  //unlimited size??
        return $product;
    }
    private static function getProductGets()                        //02     01
    {
        $product['id']      ='0|int|11';         /* => nonMandated(0)|Data type|Maxsize */
        $product['sku']     ='0|str|16';
        return $product;
    }
}