<?php
namespace Deployer;

require 'recipe/laravel.php';

// Hosts
host('nanoescrow.pro')
    ->user('axframen')
    ->port(17177)
    ->set('deploy_path', '/var/www/lumen_api');

// Main deploy task which consist of 4 other steps
task('deploy', [
    'build',
    'release',
    'cleanup',
    'success'
]);

// Installs all Lumen API dependencies on local machine
task('build', function () {
    run('composer install');
})->local();

task('release', [
    'deploy:prepare',
    'deploy:release',
    'upload',
    'deploy:shared',
    'deploy:writable',
    'deploy:symlink',
]);

// Uploads current directory from local machine to the release path on the ubuntu server machine
task('upload', function () {
    upload(__DIR__ . "/", '{{release_path}}');
});