<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/', function () use ($router) {
    //return "Here";// $router->app->version();
    return $router->app->version();
});
// $router->group(['prefix' => 'test'], function () use ($router) {
//   $router->get('list', ['uses' => 'UserController@list']);
// });
$router->group(['prefix' => 'products'], function () use ($router) {
  //posts
  $router->post('/', ['uses' => 'ProductController@createProduct']); 
  //gets
  $router->get('/', ['uses' => 'ProductController@listProducts']); 
  // $router->get('/sku/{sku}', ['uses' => 'ProductController@listProductsBySku']);
  // $router->get('/id/{id}', ['uses' => 'ProductController@listProductsById']);
  //patches
});